﻿if ($('#app').length)
{
    Vue.component('algorithm-selection', {
        props: ['algorithm'],
        template: '<a class="dropdown-item" href="#" v-on:click.prevent="$emit(\'select-algorithm\', algorithm.code)">{{ algorithm.name }}</a>'
    })

    var chart
    
    var app = new Vue({
        el: '#app',
        data: {
            isProcessing: false,
            parameters: {
                machineLearningType: null,
                trainingInput: null,
                testingInput: null,
                valueInput: null,
                useExistingModel: false,
                experimentTime: defaultExperimentTime,
                historyLength: defaultHistoryLength
            },            
            machineLearningName: 'Select an algorithm...',
            showTestingInput: false,
            showValueInput: false,
            showUseExistingModel: false,
            showExperimentTime: false,
            showHistoryLength: false,
            sampleInputs: [],
            selectedAprioriValue: null,
            algorithms: [
                {
                    code: 'Apriori',
                    name: 'Apriori',
                    showTestingInput: false,
                    showValueInput: false,
                    showUseExistingModel: false,
                    showExperimentTime: false,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'Fruit Purchases Sample',
                            trainingFileName: 'fruit-purchases.txt'
                        }
                    ]
                },
                {
                    code: 'C45',
                    name: 'C4.5',
                    showTestingInput: false,
                    showValueInput: false,
                    showUseExistingModel: false,
                    showExperimentTime: false,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'Play Tennis Sample',
                            trainingFileName: 'play-tennis.txt'
                        },
                        {
                            name: 'Iris Sample',
                            trainingFileName: 'iris.txt'
                        }
                    ]
                },
                {
                    code: 'NaiveBayes',
                    name: 'Naive Bayes',
                    showTestingInput: false,
                    showValueInput: true,
                    showUseExistingModel: false,
                    showExperimentTime: false,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'Play Tennis Sample',
                            trainingFileName: 'play-tennis.txt'
                        },
                        {
                            name: 'Iris Sample',
                            trainingFileName: 'iris.txt'
                        }
                    ]
                },
                {
                    code: 'Regression',
                    name: 'Regression',
                    showTestingInput: false,
                    showValueInput: true,
                    showUseExistingModel: false,
                    showExperimentTime: false,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'Regression Sample 1',
                            trainingFileName: 'sample-data.txt'
                        },
                        {
                            name: 'Regression Sample 2',
                            trainingFileName: 'sample-data2.txt'
                        }
                    ]
                },     
                {
                    code: 'AnomalyDetection',
                    name: 'Anomaly Detection',
                    showTestingInput: false,
                    showValueInput: false,
                    showUseExistingModel: false,
                    showExperimentTime: false,
                    showHistoryLength: true,
                    sampleInputs: [
                        {
                            name: 'Product Sales Sample',
                            trainingFileName: 'product-sales.csv'
                        }
                    ]
                },               
                {
                    code: 'AutoMLBinaryClassification',
                    name: 'AutoML - Binary Classification',
                    showTestingInput: true,
                    showValueInput: true,
                    showUseExistingModel: true,
                    showExperimentTime: true,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'Wikipedia Detox Sample',
                            trainingFileName: 'wikipedia-detox-250-line-data.tsv',
                            testingFileName: 'wikipedia-detox-250-line-test.tsv'
                        }
                    ]
                },
                {
                    code: 'AutoMLMulticlassClassification',
                    name: 'AutoML - Multiclass Classification',
                    showTestingInput: true,
                    showValueInput: true,
                    showUseExistingModel: true,
                    showExperimentTime: true,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'MNIST Sample',
                            trainingFileName: 'optdigits-train.csv',
                            testingFileName: 'optdigits-test.csv'
                        }
                    ]
                },
                {
                    code: 'AutoMLRegression',
                    name: 'AutoML - Regression',
                    showTestingInput: true,
                    showValueInput: true,
                    showUseExistingModel: true,
                    showExperimentTime: true,
                    showHistoryLength: false,
                    sampleInputs: [
                        {
                            name: 'Taxi Fare Sample',
                            trainingFileName: 'taxi-fare-train.csv',
                            testingFileName: 'taxi-fare-test.csv'
                        }
                    ]
                }
            ],
            resultReady: false,
            result: {
                aprioriResults: [
                    {
                        value1: null,
                        value2: null,
                        confidence: 0,
                        support: 0                            
                    }
                ],
                c45Result: {
                    error: 0,
                    rules: []
                },
                naiveBayesResult: {
                    probabilities: [],
                    result: null
                },
                regressionResult: {
                    linearSlope: 0,
                    linearIntercept: 0,
                    polynomialExpression: null,
                    polynomialWeights: [],
                    polynomialIntercept: 0,
                    polynomialMeanSquaredError: 0
                },
                regressionValueOutput: {
                    linearResult: 0,
                    polynomialResult: 0,
                },
                anomalyDetectionResults: [
                    {
                        timeSeriesValue: null,
                        actual: 0,
                        spikeAlert: 0,
                        spikePrediction: 0,
                        spikePValue: 0,
                        changePointAlert: 0,
                        changePointPrediction: 0,
                        changePointPValue: 0,
                        changePointMartingaleValue: 0
                    }
                ],
                autoMLRuns: [
                    {
                        runNumber: 0,
                        trainerName: null,
                        validationMetrics: {
                            accuracy: 0,
                            areaUnderRocCurve: 0,
                            areaUnderPrecisionRecallCurve: 0,
                            f1Score: 0,
                            microAccuracy: 0,
                            macroAccuracy: 0,                                
                            rSquared: 0,
                            meanAbsoluteError: 0,
                            meanSquaredError: 0,
                            rootMeanSquaredError: 0
                        },
                        runtimeInSeconds: 0
                    }
                ],
                bestAutoMLRun: {
                    runNumber: 0,
                    trainerName: null,
                    validationMetrics: {
                        accuracy: 0,
                        areaUnderRocCurve: 0,
                        areaUnderPrecisionRecallCurve: 0,
                        f1Score: 0,
                        positivePrecision: 0,
                        positiveRecall: 0,
                        negativePrecision: 0,
                        negativeRecall: 0,
                        microAccuracy: 0,
                        macroAccuracy: 0,
                        logLoss: 0,
                        perClassLogLoss1: 0,
                        perClassLogLoss2: 0,
                        perClassLogLoss3: 0,
                        lossFunction: 0,
                        rSquared: 0,
                        meanAbsoluteError: 0,
                        meanSquaredError: 0,
                        rootMeanSquaredError: 0
                    },
                    runtimeInSeconds: 0
                },
                autoMLBinaryValueOutput: {
                    result: null,
                    score: null
                },
                autoMLMulticlassClassificationValueOutput: {
                    multiclassClassificationScores: [
                        {
                            value: null,
                            score: 0
                        }
                    ]
                },
                autoMLRegressionValueOutput: {
                    amount: 0
                }
            }
        },
        methods: {
            process: function () {
                this.isProcessing = true
                this.resultReady = false

                $.ajax(
                    {
                        url: baseUrl + 'Home/Process', 
                        type: 'POST',
                        data: this.parameters,
                        success: function(result) {
                            app.result = result
                            app.resultReady = true
                            app.isProcessing = false

                            app.drawCharts()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            app.isProcessing = false
                        }
                    }
                )
            },
            getTrainingFileContent: function (fileName) {
                this.getFileContent('training', fileName)
            },
            getTestingFileContent: function (fileName) {
                this.getFileContent('testing', fileName)
            },
            getFileContent: function (inputType, fileName) {
                $.ajax(
                    {
                        url: baseUrl + 'Home/GetFileContent', 
                        data: {
                            fileName: this.parameters.machineLearningType + '/' + fileName
                        },
                        inputType: inputType,
                        success: function(result) {
                            if (this.inputType == 'training') {
                                app.parameters.trainingInput = result
                            }
                            else if (this.inputType == 'testing') {
                                app.parameters.testingInput = result
                            }
                        }
                    }
                )
            },
            selectAlgorithm: function (machineLearningType) {
                let selection = this.algorithms.filter(item => {
                    return item.code == machineLearningType
                })
                
                if (selection.length) {
                    this.parameters.machineLearningType = selection[0].code
                    this.machineLearningName = selection[0].name
                    this.showTestingInput = selection[0].showTestingInput
                    this.showValueInput = selection[0].showValueInput
                    this.showUseExistingModel = selection[0].showUseExistingModel
                    this.showExperimentTime = selection[0].showExperimentTime
                    this.showHistoryLength = selection[0].showHistoryLength
                    this.sampleInputs = selection[0].sampleInputs
                    this.resultReady = false
                    window.location.hash = machineLearningType
                }     
                else {
                    this.parameters.machineLearningType = null
                    this.resultReady = false
                }               
            },
            loadSampleInput: function (sampleInput) {
                if (sampleInput.trainingFileName) {
                    this.getTrainingFileContent(sampleInput.trainingFileName)
                }

                if (sampleInput.testingFileName) {
                    this.getTestingFileContent(sampleInput.testingFileName)
                }
            },
            getActualChartData: function () {
                let splitCharacter = ','
                if (this.parameters.trainingInput.indexOf('\t') > -1) {
                    splitCharacter = '\t'
                }

                let xValues = []
                let yValues = []
                let lines = this.parameters.trainingInput.split('\n')
                lines.forEach(function(item, index) {
                    let values = item.split(splitCharacter)
                    if (!isNaN(values[0]) && !isNaN(values[1])) {
                        xValues.push(parseFloat(values[0]).toFixed(2))
                        yValues.push(parseFloat(values[1]).toFixed(2))
                    }
                })

                return { xValues, yValues }
            },
            getLinearRegressionData: function (xValues) {
                let slope = this.result.regressionResult.linearSlope
                let intercept = this.result.regressionResult.linearIntercept
                
                let yValues = []
                xValues.forEach(function(item, index) {
                    yValues.push(((slope * item) + intercept).toFixed(2))
                })

                return { xValues, yValues }
            },
            getPolynomialRegressionData: function (xValues) {
                let weights = this.result.regressionResult.polynomialWeights
                let intercept = this.result.regressionResult.polynomialIntercept
                
                let yValues = []
                xValues.forEach(function(item, index) {
                    yValues.push(((weights[0] * (item * item)) + (weights[1] * item) + intercept).toFixed(2))
                })

                return { xValues, yValues }
            },
            drawCharts: function () {
                this.drawRegressionChart()
                this.drawAnomalyDetectionChart()
            },
            drawRegressionChart: function () {
                if (this.parameters.machineLearningType == 'Regression'
                    && this.result.regressionResult) {
                    $('#regressionChart').show()
                    let ctx = document.getElementById('regressionChart').getContext('2d')
                    let actualData = this.getActualChartData()
                    let linearData = this.getLinearRegressionData(actualData.xValues)
                    let polynomialData = this.getPolynomialRegressionData(actualData.xValues)
                    if (chart) {
                        chart.destroy()
                    }
                    chart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: actualData.xValues,
                            datasets: [
                                {
                                    label: 'Actual',
                                    backgroundColor: 'navy',
                                    borderColor: 'navy',
                                    data: actualData.yValues,
                                    fill: false
                                },
                                {
                                    label: 'Linear Regression',
                                    backgroundColor: 'green',
                                    borderColor: 'green',
                                    data: linearData.yValues,
                                    fill: false,
                                    lineTension: 0
                                },
                                {
                                    label: 'Polynomial Regression',
                                    backgroundColor: 'purple',
                                    borderColor: 'purple',
                                    data: polynomialData.yValues,
                                    fill: false
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Regression'
                            },
                            tooltips: {
                                mode: 'index'
                            }
                        }
                    })
                }
                else {
                    $('#regressionChart').hide()
                }
            },
            getAnomalyChartData: function () {
                let data = this.result.anomalyDetectionResults
                
                let xValues = []
                let yValues = []
                data.forEach(function(item, index) {
                    xValues.push(item.timeSeriesValue)
                    yValues.push(item.actual.toFixed(2))
                })

                return { xValues, yValues }
            },
            drawAnomalyDetectionChart: function () {
                if (this.parameters.machineLearningType == 'AnomalyDetection'
                    && this.result.anomalyDetectionResults 
                    && this.result.anomalyDetectionResults.length) {
                    $('#anomalyDetectionChart').show()
                    let ctx = document.getElementById('anomalyDetectionChart').getContext('2d')
                    let actualData = this.getAnomalyChartData()
                    if (chart) {
                        chart.destroy()
                    }
                    chart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: actualData.xValues,
                            datasets: [
                                {
                                    label: 'Data',
                                    backgroundColor: 'navy',
                                    borderColor: 'navy',
                                    data: actualData.yValues,
                                    fill: false
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Anomaly Detection'
                            },
                            tooltips: {
                                mode: 'index',
                                callbacks: {
                                    // Use the footer callback to display the sum of the items showing in the tooltip
                                    beforeTitle: function(ctx, data) {
                                        let isSpike = app.result.anomalyDetectionResults[ctx[0].index].spikeAlert
                                        let isChangePoint = app.result.anomalyDetectionResults[ctx[0].index].changePointAlert
            
                                        if (isSpike && isChangePoint) {
                                            return 'Spike & Change Point'
                                        }
                                        else if (isSpike) {
                                            return 'Spike'
                                        }
                                        else if (isChangePoint) {
                                            return 'Change Point'
                                        }
                                        else {
                                            return ''
                                        }
                                    },
                                }
                            },
                            elements: {
                                point: {
                                    radius: function (ctx) {
                                        let isSpike = app.result.anomalyDetectionResults[ctx.dataIndex].spikeAlert
                                        let isChangePoint = app.result.anomalyDetectionResults[ctx.dataIndex].changePointAlert
                                        
                                        if (isSpike && isChangePoint) {
                                            return 15
                                        }
                                        else if (isSpike) {
                                            return 10
                                        }
                                        else if (isChangePoint) {
                                            return 10
                                        }
                                        else {
                                            return 1
                                        }
                                    },
                                    pointStyle: function (ctx) {
                                        let isSpike = app.result.anomalyDetectionResults[ctx.dataIndex].spikeAlert
                                        let isChangePoint = app.result.anomalyDetectionResults[ctx.dataIndex].changePointAlert
                                        if (isSpike && isChangePoint) {
                                            return 'star'
                                        }
                                        else if (isSpike) {
                                            return 'triangle'
                                        }
                                        else if (isChangePoint) {
                                            return 'rectRot'
                                        }
                                        else {
                                            return 'circle'
                                        }
                                    },
                                    hoverRadius: 15
                                }
                            }
                        }
                    })

                    $('#anomalyDetectionChartLegend').show()
                }
                else {
                    $('#anomalyDetectionChart').hide()
                    $('#anomalyDetectionChartLegend').hide()
                }
            },
            scrollToSampleInput: function () {
                $("#sampleInputPanel").parent().find("ol")[0].style = "border: 2px solid lightblue"
                
                $('html, body').animate({
                    scrollTop: $("#sampleInputPanel").offset().top - 30
                }, 500)

                setTimeout(function () {
                    $("#sampleInputPanel").parent().find("ol")[0].style = ""
                }, 2000)
            }
        },
        computed: {
            uniqueAprioriResults: function () {
                let results = []
                for (i = 0; i < this.result.aprioriResults.length; i++) {
                    results.push(this.result.aprioriResults[i].value1)
                }

                function onlyUnique(value, index, self) { 
                    return self.indexOf(value) === index
                }             

                let unique = results.filter(onlyUnique)              
                return unique
            },
            aprioriSelectedResults: function () {
                let selectedAprioriValue = this.selectedAprioriValue
                function matches(value, index, self) { 
                    return value.value1 == selectedAprioriValue
                }             

                let selectedResults = this.result.aprioriResults.filter(matches)              
                return selectedResults
            }
        }
    })

    $(function() {
        $('#app').show()

        let hash = window.location.hash.substr(1)
        if (hash) {
            app.selectAlgorithm(hash)
        }
    })
}