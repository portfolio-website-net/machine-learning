﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MachineLearning.Models;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;

namespace MachineLearning.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMachineLearningService _machineLearningService;
        private readonly IFileHelperService _fileHelperService;

        public HomeController(ILogger<HomeController> logger,
            IMachineLearningService machineLearningService, 
            IFileHelperService fileHelperService)
        {
            _logger = logger;
            _machineLearningService = machineLearningService;
            _fileHelperService = fileHelperService;
        }

        public IActionResult Index()
        {            
            return View();
        }

        [AutoValidateAntiforgeryToken]        
        public IActionResult Process(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            return Json(_machineLearningService.Process(machineLearningServiceParameters));
        }        

        public IActionResult GetFileContent(string fileName)
        {
            return Content(_fileHelperService.GetFileContent(fileName));
        }    

        public IActionResult GetProjectSourceZipBase64()
        {
            return Content(_fileHelperService.GetProjectSourceZipBase64());                    
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
