﻿namespace MachineLearning
{
    public class Settings
    {
        public int DefaultExperimentTime { get; set; }

        public int DefaultHistoryLength { get; set; }
    }
}
