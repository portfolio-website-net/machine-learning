﻿namespace MachineLearning.Services.Types
{
    public enum MachineLearningType
    {
        Apriori,
        C45,
        NaiveBayes,
        Regression,
        AnomalyDetection,
        AutoMLBinaryClassification,
        AutoMLMulticlassClassification,
        AutoMLRegression
    }
}
