﻿using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Types;
using MachineLearning.Services.Algorithms;
using MachineLearning.Services.Objects;

namespace MachineLearning.Services.Factories
{
    public class AlgorithmFactory
    {
        public IAlgorithm Initialize(MachineLearningServiceParameters machineLearningServiceParameters, Settings settings)
        {
            IAlgorithm algorithm = null;
            if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.Apriori.ToString())
            {
                algorithm = new Apriori(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.C45.ToString())
            {
                algorithm = new C45(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.NaiveBayes.ToString())
            {
                algorithm = new NaiveBayes(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.Regression.ToString())
            {
                algorithm = new Regression(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.AnomalyDetection.ToString())
            {
                algorithm = new AnomalyDetection(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.AutoMLBinaryClassification.ToString())
            {
                algorithm = new AutoMLBinaryClassification(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.AutoMLMulticlassClassification.ToString())
            {
                algorithm = new AutoMLMulticlassClassification(machineLearningServiceParameters);
            }
            else if (machineLearningServiceParameters.MachineLearningType == MachineLearningType.AutoMLRegression.ToString())
            {
                algorithm = new AutoMLRegression(machineLearningServiceParameters);
            }

            return algorithm;
        }
    }
}
