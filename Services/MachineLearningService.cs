﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Factories;
using MachineLearning.Services.Objects;

namespace MachineLearning.Services
{
    public class MachineLearningService : IMachineLearningService
    {        
        private readonly Settings _settings;

        public MachineLearningService(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }        

        public MachineLearningResult Process(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            var factory = new AlgorithmFactory().Initialize(machineLearningServiceParameters, _settings);   

            try
            {
                return factory.GetResult();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
            return new MachineLearningResult();
        }
    }
}
