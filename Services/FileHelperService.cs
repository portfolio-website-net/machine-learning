﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using System.IO.Compression;
using MachineLearning.Services.Interfaces;

namespace MachineLearning.Services
{
    public class FileHelperService : IFileHelperService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public FileHelperService(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public string GetFileContent(string fileName)
        {
            var projectRootPath = _webHostEnvironment.ContentRootPath;
            var basePath = Path.Combine(projectRootPath, "SampleData");
            var fileContent = string.Join("\n", System.IO.File.ReadAllLines(Path.Combine(basePath, fileName)));
            return fileContent;
        }    

        public string GetProjectSourceZipBase64()
        {
            var projectRootPath = _webHostEnvironment.ContentRootPath;
            var files = Directory.GetFiles(projectRootPath, "*.*", SearchOption.AllDirectories);     
            var excludeFolders = new string[] { "bin", "obj", ".git", "SampleData" };

            using (var memoryStream = new MemoryStream())
            {
                using (var zip = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {                    
                    foreach (var file in files)
                    {
                        var fileName = file.Replace(projectRootPath + "\\", string.Empty);
                        if (!excludeFolders.Any(x => System.IO.Path.GetDirectoryName(fileName).Contains(x + "\\")
                            || System.IO.Path.GetDirectoryName(fileName) == x))
                        {
                            using (var source = new FileStream(file, FileMode.Open, FileAccess.Read))
                            {                                                        
                                var entry = zip.CreateEntry(fileName);
                                using (var entryStream = entry.Open())
                                {
                                    using (var bw = new BinaryWriter(entryStream))
                                    {
                                        byte[] buffer = new byte[2048];
                                        int bytesRead;
                                        while((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0) 
                                        {
                                            bw.Write(buffer, 0, bytesRead);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                memoryStream.Seek(0, SeekOrigin.Begin);
                var bytes = memoryStream.ToArray();
                string base64 = Convert.ToBase64String(bytes);
                return $"cat > machine-learning.zip.base64 <<EOL\n{base64}\nEOL";
            }            
        }
    }
}
