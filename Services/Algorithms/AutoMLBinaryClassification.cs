﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;
using System.IO;

namespace MachineLearning.Services.Algorithms
{
    public class AutoMLBinaryClassification : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public AutoMLBinaryClassification(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }   

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);
            var testingDataset = ParseInput.Parse(_machineLearningServiceParameters.TestingInput);

            var trainingList = new List<InputData>();
            
            foreach (var item in trainingDataset.Skip(1))
            {
                trainingList.Add(new InputData
                {
                    Label = item[0] == 1.ToString(),
                    Text = item[1].ToString()
                });
            }

            var testingList = new List<InputData>();
            foreach (var item in testingDataset.Skip(1))
            {
                testingList.Add(new InputData
                {
                    Label = item[0] == 1.ToString(),
                    Text = item[1].ToString()
                });
            }            

            var mlContext = new MLContext();            
            var trainingDataView = mlContext.Data.LoadFromEnumerable(trainingList);
            var testingDataView = mlContext.Data.LoadFromEnumerable(testingList);

            ITransformer trainedModel;
            DataViewSchema modelInputSchema;
            var autoMLRuns = new List<AutoMLRun>();
            var bestAutoMLRun = new AutoMLRun();

            var modelPath = PathHelper.GetAbsolutePath(@"Models\AutoMLBinaryClassification.zip");
            if (!_machineLearningServiceParameters.UseExistingModel
                || !File.Exists(modelPath))
            {
                var experimentResult = mlContext.Auto()
                    .CreateBinaryClassificationExperiment(_machineLearningServiceParameters.ExperimentTime)
                    .Execute(trainingDataView);

                var topRuns = experimentResult.RunDetails
                    .Where(r => r.ValidationMetrics != null && !double.IsNaN(r.ValidationMetrics.Accuracy))
                    .OrderByDescending(r => r.ValidationMetrics.Accuracy).Take(3);

                autoMLRuns = new List<AutoMLRun>();
                for (var i = 0; i < topRuns.Count(); i++)
                {
                    var run = topRuns.ElementAt(i);
                    autoMLRuns.Add(new AutoMLRun
                    {
                        RunNumber = i + 1,
                        TrainerName = run.TrainerName,
                        ValidationMetrics = new ValidationMetric {
                            Accuracy = run.ValidationMetrics.Accuracy,
                            AreaUnderRocCurve = run.ValidationMetrics.AreaUnderRocCurve,
                            AreaUnderPrecisionRecallCurve = run.ValidationMetrics.AreaUnderPrecisionRecallCurve,
                            F1Score = run.ValidationMetrics.F1Score
                        },
                        RuntimeInSeconds = run.RuntimeInSeconds
                    });                
                }

                var bestRun = experimentResult.BestRun;
                trainedModel = bestRun.Model;
                var predictions = trainedModel.Transform(testingDataView);
                var metrics = mlContext.BinaryClassification.EvaluateNonCalibrated(data:predictions, scoreColumnName: "Score");
                
                bestAutoMLRun = new AutoMLRun
                {
                    TrainerName = bestRun.TrainerName,
                    ValidationMetrics = new ValidationMetric {
                        Accuracy = metrics.Accuracy,
                        AreaUnderRocCurve = metrics.AreaUnderRocCurve,
                        AreaUnderPrecisionRecallCurve = metrics.AreaUnderPrecisionRecallCurve,
                        F1Score = metrics.F1Score,
                        PositivePrecision = metrics.PositivePrecision,
                        PositiveRecall = metrics.PositiveRecall,
                        NegativePrecision = metrics.NegativePrecision,
                        NegativeRecall = metrics.NegativeRecall
                    },
                    RuntimeInSeconds = bestRun.RuntimeInSeconds
                };

                modelInputSchema = trainingDataView.Schema;
                mlContext.Model.Save(trainedModel, modelInputSchema, modelPath);
            }
            else
            {
                trainedModel = mlContext.Model.Load(modelPath, out modelInputSchema);
            }

            var autoMLBinaryValueOutput = PredictResult(
                mlContext,
                trainedModel,
                modelInputSchema,
                _machineLearningServiceParameters.ValueInput);            

            return new MachineLearningResult
            {
                AutoMLRuns = autoMLRuns,
                BestAutoMLRun = bestAutoMLRun,
                AutoMLBinaryValueOutput = autoMLBinaryValueOutput
            };
        }

        private AutoMLBinaryValueOutput PredictResult(
            MLContext mlContext,
            ITransformer trainedModel,
            DataViewSchema modelInputSchema,
            string valueInput)
        {            
            var autoMLBinaryValueOutput = new AutoMLBinaryValueOutput();
            var inputValue = new InputData
            {
                Text = valueInput
            };

            var predictionEngine = mlContext.Model.CreatePredictionEngine<InputData, OutputData>(trainedModel);
            var predictedResult = predictionEngine.Predict(inputValue);

            autoMLBinaryValueOutput = new AutoMLBinaryValueOutput
            {
                Result = predictedResult.Result,
                Score = predictedResult.Score
            };

            return autoMLBinaryValueOutput;
        }

        private class InputData
        {
            public bool Label { get; set; }

            public string Text { get; set; }
        }

        private class OutputData
        {
            [ColumnName("PredictedLabel")]
            public bool Result { get; set; }

            public float Score { get; set; }
        }        
    }
}
