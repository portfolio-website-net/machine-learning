﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;

namespace MachineLearning.Services.Algorithms
{
    public class Apriori : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public Apriori(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var dataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);
            var apriori = new Apriori<string>(threshold: 2, confidence: 0.7);
            var classifier = apriori.Learn(dataset);
            var rules = classifier.Rules;

            var aprioriResults = new List<AprioriResult>();
            foreach (var rule in rules)
            {
                aprioriResults.Add(new AprioriResult
                {
                    Value1 = string.Join(", ", rule.X),
                    Value2 = string.Join(", ", rule.Y),                    
                    Support = rule.Support,
                    Confidence = rule.Confidence
                });
            }

            return new MachineLearningResult
            {
                AprioriResults = aprioriResults
            };
        }
    }
}
