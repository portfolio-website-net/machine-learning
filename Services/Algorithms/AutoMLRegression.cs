﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;
using System.IO;

namespace MachineLearning.Services.Algorithms
{
    public class AutoMLRegression : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public AutoMLRegression(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);
            var testingDataset = ParseInput.Parse(_machineLearningServiceParameters.TestingInput);
            var valueInputDataset = ParseInput.Parse(_machineLearningServiceParameters.ValueInput);

            var trainingList = new List<InputData>();
            
            foreach (var item in trainingDataset.Skip(1))
            {
                trainingList.Add(new InputData
                {
                    Id = item[0].ToString(),
                    Code = item[1].ToString(),
                    Value1 = float.Parse(item[2]),
                    Value2 = float.Parse(item[3]),
                    Value3 = float.Parse(item[4]),
                    Type = item[5].ToString(),
                    Amount = float.Parse(item[6])
                });
            }

            var testingList = new List<InputData>();
            foreach (var item in testingDataset.Skip(1))
            {
                testingList.Add(new InputData
                {
                     Id = item[0].ToString(),
                    Code = item[1].ToString(),
                    Value1 = float.Parse(item[2]),
                    Value2 = float.Parse(item[3]),
                    Value3 = float.Parse(item[4]),
                    Type = item[5].ToString(),
                    Amount = float.Parse(item[6])
                });
            }

            var mlContext = new MLContext();            
            var trainingDataView = mlContext.Data.LoadFromEnumerable(trainingList);
            var testingDataView = mlContext.Data.LoadFromEnumerable(testingList);

            ITransformer trainedModel;
            DataViewSchema modelInputSchema;
            var autoMLRuns = new List<AutoMLRun>();
            var bestAutoMLRun = new AutoMLRun();

            var modelPath = PathHelper.GetAbsolutePath(@"Models\AutoMLRegression.zip");
            if (!_machineLearningServiceParameters.UseExistingModel
                || !File.Exists(modelPath))
            {
                var experimentResult = mlContext.Auto()
                    .CreateRegressionExperiment(_machineLearningServiceParameters.ExperimentTime)
                    .Execute(trainingDataView, "Amount");

                var topRuns = experimentResult.RunDetails
                    .Where(r => r.ValidationMetrics != null && !double.IsNaN(r.ValidationMetrics.RSquared))
                    .OrderByDescending(r => r.ValidationMetrics.RSquared).Take(3);

                autoMLRuns = new List<AutoMLRun>();
                for (var i = 0; i < topRuns.Count(); i++)
                {
                    var run = topRuns.ElementAt(i);
                    autoMLRuns.Add(new AutoMLRun
                    {
                        RunNumber = i + 1,
                        TrainerName = run.TrainerName,
                        ValidationMetrics = new ValidationMetric {
                            RSquared = run.ValidationMetrics.RSquared,
                            MeanAbsoluteError = run.ValidationMetrics.MeanAbsoluteError,
                            MeanSquaredError = run.ValidationMetrics.MeanSquaredError,
                            RootMeanSquaredError = run.ValidationMetrics.RootMeanSquaredError
                        },
                        RuntimeInSeconds = run.RuntimeInSeconds
                    });                
                }

                var bestRun = experimentResult.BestRun;
                trainedModel = bestRun.Model;
                var predictions = trainedModel.Transform(testingDataView);
                var metrics = mlContext.Regression.Evaluate(predictions, labelColumnName: "Amount", scoreColumnName: "Score");
                
                bestAutoMLRun = new AutoMLRun
                {
                    TrainerName = bestRun.TrainerName,
                    ValidationMetrics = new ValidationMetric {
                        LossFunction = bestRun.ValidationMetrics.LossFunction,
                        RSquared = bestRun.ValidationMetrics.RSquared,
                        MeanAbsoluteError = bestRun.ValidationMetrics.MeanAbsoluteError,
                        MeanSquaredError = bestRun.ValidationMetrics.MeanSquaredError,
                        RootMeanSquaredError = bestRun.ValidationMetrics.RootMeanSquaredError
                    },
                    RuntimeInSeconds = bestRun.RuntimeInSeconds
                };

                modelInputSchema = trainingDataView.Schema;
                mlContext.Model.Save(trainedModel, modelInputSchema, modelPath);
            }
            else
            {
                trainedModel = mlContext.Model.Load(modelPath, out modelInputSchema);
            }

            var autoMLRegressionValueOutput = PredictResult(
                mlContext,
                trainedModel,
                modelInputSchema,
                valueInputDataset);
            
            return new MachineLearningResult
            {
                AutoMLRuns = autoMLRuns,
                BestAutoMLRun = bestAutoMLRun,
                AutoMLRegressionValueOutput = autoMLRegressionValueOutput
            };
        }

        private AutoMLRegressionValueOutput PredictResult(
            MLContext mlContext,
            ITransformer trainedModel,
            DataViewSchema modelInputSchema,
            string[][] valueInputDataset)
        {            
            var autoMLRegressionValueOutput = new AutoMLRegressionValueOutput();
            if (valueInputDataset.FirstOrDefault() != null)
            {
                var valueInput = valueInputDataset.First();
                var inputValue = new InputData
                {
                    Id = valueInput[0].ToString(),
                    Code = valueInput[1].ToString(),
                    Value1 = float.Parse(valueInput[2]),
                    Value2 = float.Parse(valueInput[3]),
                    Value3 = float.Parse(valueInput[4]),
                    Type = valueInput[5].ToString()
                };               

                var predictionEngine = mlContext.Model.CreatePredictionEngine<InputData, OutputData>(trainedModel);
                var predictedResult = predictionEngine.Predict(inputValue);

                autoMLRegressionValueOutput = new AutoMLRegressionValueOutput
                {
                    Amount = predictedResult.Amount
                };
            }

            return autoMLRegressionValueOutput;
        }

        private class InputData
        {
            public string Id;

            public string Code;

            public float Value1;

            public float Value2;

            public float Value3;

            public string Type;

            public float Amount;
        }

        private class OutputData
        {
            [ColumnName("Score")]
            public float Amount = 0;
        }
    }
}
