﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;
using System.IO;

namespace MachineLearning.Services.Algorithms
{
    public class AutoMLMulticlassClassification : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        private Dictionary<string, float> _valueResultDictionary = new Dictionary<string, float>();

        public AutoMLMulticlassClassification(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);
            var testingDataset = ParseInput.Parse(_machineLearningServiceParameters.TestingInput);
            var valueInputDataset = ParseInput.Parse(_machineLearningServiceParameters.ValueInput);

            var trainingList = new List<InputData>();
            
            var inputCountMaximum = 64 + 1;            
            trainingDataset = PadItemValuesWithZeros(trainingDataset, inputCountMaximum);

            foreach (var item in trainingDataset)
            {
                trainingList.Add(new InputData
                {
                    InputValues = item.Select(x => float.Parse(x)).Take(inputCountMaximum - 1).ToArray(),
                    Result = item.Select(x => GetFloatForResult(x.ToString())).Skip(inputCountMaximum - 1).First()
                });
            }
            
            var testingList = new List<InputData>();

            testingDataset = PadItemValuesWithZeros(testingDataset, inputCountMaximum);

            foreach (var item in testingDataset)
            {
                testingList.Add(new InputData
                {
                    InputValues = item.Select(x => float.Parse(x)).Take(inputCountMaximum - 1).ToArray(),
                    Result = item.Select(x =>  GetFloatForResult(x.ToString())).Skip(inputCountMaximum - 1).First()
                });
            }

            var mlContext = new MLContext();            
            var trainingDataView = mlContext.Data.LoadFromEnumerable(trainingList);
            var testingDataView = mlContext.Data.LoadFromEnumerable(testingList);

            ITransformer trainedModel;
            DataViewSchema modelInputSchema;
            var autoMLRuns = new List<AutoMLRun>();
            var bestAutoMLRun = new AutoMLRun();

            var modelPath = PathHelper.GetAbsolutePath(@"Models\AutoMLMulticlassClassification.zip");
            if (!_machineLearningServiceParameters.UseExistingModel
                || !File.Exists(modelPath))
            {
                var experimentResult = mlContext.Auto()
                    .CreateMulticlassClassificationExperiment(_machineLearningServiceParameters.ExperimentTime)
                    .Execute(trainingDataView, "Result");

                var topRuns = experimentResult.RunDetails
                    .Where(r => r.ValidationMetrics != null && !double.IsNaN(r.ValidationMetrics.MicroAccuracy))
                    .OrderByDescending(r => r.ValidationMetrics.MicroAccuracy).Take(3);
                
                for (var i = 0; i < topRuns.Count(); i++)
                {
                    var run = topRuns.ElementAt(i);
                    autoMLRuns.Add(new AutoMLRun
                    {
                        RunNumber = i + 1,
                        TrainerName = run.TrainerName,
                        ValidationMetrics = new ValidationMetric {
                            MicroAccuracy = run.ValidationMetrics.MicroAccuracy,
                            MacroAccuracy = run.ValidationMetrics.MacroAccuracy
                        },
                        RuntimeInSeconds = run.RuntimeInSeconds
                    });                
                }

                var bestRun = experimentResult.BestRun;
                trainedModel = bestRun.Model;
                var predictions = trainedModel.Transform(testingDataView);
                var metrics = mlContext.MulticlassClassification.Evaluate(data:predictions, labelColumnName: "Result", scoreColumnName: "Score");
                
                bestAutoMLRun = new AutoMLRun
                {
                    TrainerName = bestRun.TrainerName,
                    ValidationMetrics = new ValidationMetric {
                        MicroAccuracy = metrics.MicroAccuracy,
                        MacroAccuracy = metrics.MacroAccuracy,
                        LogLoss = metrics.LogLoss,
                        PerClassLogLoss1 = metrics.PerClassLogLoss.Count() > 0 ? metrics.PerClassLogLoss[0] : 0,
                        PerClassLogLoss2 = metrics.PerClassLogLoss.Count() > 1 ? metrics.PerClassLogLoss[1] : 0,
                        PerClassLogLoss3 = metrics.PerClassLogLoss.Count() > 2 ? metrics.PerClassLogLoss[2] : 0
                    },
                    RuntimeInSeconds = bestRun.RuntimeInSeconds
                };

                modelInputSchema = trainingDataView.Schema;
                mlContext.Model.Save(trainedModel, modelInputSchema, modelPath);
            }
            else
            {
                trainedModel = mlContext.Model.Load(modelPath, out modelInputSchema);
            }

            var autoMLMulticlassClassificationValueOutput = PredictResult(
                mlContext,
                trainedModel,
                modelInputSchema,
                valueInputDataset);

            return new MachineLearningResult
            {
                AutoMLRuns = autoMLRuns,
                BestAutoMLRun = bestAutoMLRun,
                AutoMLMulticlassClassificationValueOutput = autoMLMulticlassClassificationValueOutput
            };
        }      

        private float GetFloatForResult(string result)  
        {
            if (!_valueResultDictionary.ContainsKey(result))
            {
                _valueResultDictionary.Add(result, _valueResultDictionary.Count);
            }
            
            return _valueResultDictionary[result];
        }

        private AutoMLMulticlassClassificationValueOutput PredictResult(
            MLContext mlContext,
            ITransformer trainedModel,
            DataViewSchema modelInputSchema,
            string[][] valueInputDataset)
        {            
            var autoMLMulticlassClassificationValueOutput = new AutoMLMulticlassClassificationValueOutput();
            if (valueInputDataset.FirstOrDefault() != null)
            {
                var inputCountMaximum = 64;

                valueInputDataset = PadItemValuesWithZeros(valueInputDataset, inputCountMaximum);                

                var inputValue = new InputData
                {
                    InputValues = valueInputDataset.First().Select(x => float.Parse(x)).Take(inputCountMaximum).ToArray()
                };               

                var predictionEngine = mlContext.Model.CreatePredictionEngine<InputData, OutputData>(trainedModel);

                // Get the key value mapping for Result to Score index
                var keyValues = default(VBuffer<float>);
                trainedModel.GetOutputSchema(modelInputSchema)["Result"].GetKeyValues<float>(ref keyValues);
                var keys = keyValues.Items().ToDictionary(x => (int)x.Value, x => x.Key);

                try
                {
                    var predictedResult = predictionEngine.Predict(inputValue);

                    var multiclassClassificationScores = new List<MulticlassClassificationScore>();
                    for (var i = 0; i < keys.Count; i++)
                    {                    
                        var item = keys.ElementAt(i);
                        multiclassClassificationScores.Add(new MulticlassClassificationScore
                        {
                            Value = _valueResultDictionary.Where(x => x.Value == item.Key).Select(x => x.Key).First(), 
                            Score = predictedResult.Score[i]
                        });
                    }

                    multiclassClassificationScores = multiclassClassificationScores.OrderBy(x => x.Value).ToList();

                    autoMLMulticlassClassificationValueOutput = new AutoMLMulticlassClassificationValueOutput
                    {
                        MulticlassClassificationScores = multiclassClassificationScores
                    };
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }                
            }

            return autoMLMulticlassClassificationValueOutput;
        }

        private string[][] PadItemValuesWithZeros(string[][] input, int size)
        {
            var paddedInput = new List<string[]>();
            foreach (var item in input)
            {
                if (item.Count() < size)
                {
                    var paddedItem = item.ToList();
                    while (paddedItem.Count() < size)
                    {
                        paddedItem.Insert(0, "0");
                    }

                    paddedInput.Add(paddedItem.ToArray());
                }
                else
                {
                    paddedInput.Add(item);
                }                
            }

            return paddedInput.ToArray();
        }

        private class InputData
        {
            [VectorType(64)]
            public float[] InputValues;

            public float Result;
        }

        private class OutputData
        {
            public float[] Score = new float[0];
        }
    }
}
