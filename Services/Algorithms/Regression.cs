﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;

namespace MachineLearning.Services.Algorithms
{
    public class Regression : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public Regression(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);
            double.TryParse(_machineLearningServiceParameters.ValueInput, out double valueInput);

            var xValues = new List<double>();
            var yValues = new List<double>();
            foreach (var item in trainingDataset)
            {
                xValues.Add(double.Parse(item[0]));
                yValues.Add(double.Parse(item[1]));
            }

            var ols = new OrdinaryLeastSquares();
            var linearRegression = ols.Learn(xValues.ToArray(), yValues.ToArray());
            var linearSlope = linearRegression.Slope;
            var linearIntercept = linearRegression.Intercept;

            var ls = new PolynomialLeastSquares()
            {
                Degree = 2
            };

            PolynomialRegression polynomialRegression = ls.Learn(xValues.ToArray(), yValues.ToArray());
            string polynomialExpression = polynomialRegression.ToString("N1");

            double[] polynomialWeights = polynomialRegression.Weights;
            var polynomialIntercept = polynomialRegression.Intercept;

            double[] pred = polynomialRegression.Transform(xValues.ToArray());
            double polynomialMeanSquaredError = new SquareLoss(yValues.ToArray()).Loss(pred);

            return new MachineLearningResult
            {
                RegressionResult = new RegressionResult
                {
                    LinearSlope = linearSlope,
                    LinearIntercept = linearIntercept,
                    PolynomialExpression = polynomialExpression,
                    PolynomialWeights = polynomialWeights,
                    PolynomialIntercept = polynomialIntercept,
                    PolynomialMeanSquaredError = polynomialMeanSquaredError
                },
                RegressionValueOutput = new RegressionValueOutput
                {
                    LinearResult = linearRegression.Transform(valueInput),
                    PolynomialResult = polynomialRegression.Transform(valueInput),
                }
            };
        }
    }
}
