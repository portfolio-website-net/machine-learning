﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;

namespace MachineLearning.Services.Algorithms
{
    public class C45 : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public C45(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);

            var columnNames = trainingDataset.Take(1).First().Select(x => x.ToString()).ToArray();
            var data = ParseInput.ConvertToDataTable(columnNames, trainingDataset);            

            var codebook = new Codification()
            {
                DefaultMissingValueReplacement = Double.NaN
            };

            codebook.Learn(data);
            var symbols = codebook.Apply(data);
            var inputNames = columnNames.Take(columnNames.Count() - 1).ToArray();            
            double[][] inputs = symbols.ToJagged(inputNames);
            int[] outputs = symbols.ToArray<int>(columnNames.Last());

            var teacher = new Accord.MachineLearning.DecisionTrees.Learning.C45Learning()
            {
                Attributes = DecisionVariable.FromCodebook(codebook, inputNames)
            };

            DecisionTree decisionTree = teacher.Learn(inputs, outputs);
            int[] predicted = decisionTree.Decide(inputs);
            double error = new ZeroOneLoss(outputs).Loss(predicted);
            DecisionSet rules = decisionTree.ToRules();

            string ruleText = rules.ToString(codebook, columnNames.Last(),
                System.Globalization.CultureInfo.InvariantCulture);

            return new MachineLearningResult
            {
                C45Result = new C45Result
                {
                    Error = error,
                    Rules = ruleText.Split('\n').Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x)).ToList()
                }
            };
        }        
    }
}
