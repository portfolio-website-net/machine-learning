﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;

namespace MachineLearning.Services.Algorithms
{
    public class AnomalyDetection : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public AnomalyDetection(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);

            var trainingList = new List<InputData>();
            
            foreach (var item in trainingDataset.Skip(1))
            {
                trainingList.Add(new InputData
                {
                    TimeSeriesValue = item[0].ToString(),
                    Actual = float.Parse(item[1].ToString())
                });
            }

            var mlContext = new MLContext();            
            var trainingDataView = mlContext.Data.LoadFromEnumerable(trainingList);

            var spikeDetectionPipeline = mlContext.Transforms.DetectIidSpike(
                outputColumnName: nameof(OutputData.Prediction), 
                inputColumnName: nameof(InputData.Actual),
                confidence: 95, 
                pvalueHistoryLength: _machineLearningServiceParameters.HistoryLength);
            
            var changePointDetectionPipeline = mlContext.Transforms.DetectIidChangePoint(
                outputColumnName: nameof(OutputData.Prediction), 
                inputColumnName: nameof(InputData.Actual), 
                confidence: 95, 
                changeHistoryLength: _machineLearningServiceParameters.HistoryLength);

            var spikeDetectionModel = spikeDetectionPipeline.Fit(trainingDataView);
            var changePointDetectionModel = changePointDetectionPipeline.Fit(trainingDataView);

            var spikeDetectionTransformed = spikeDetectionModel.Transform(trainingDataView);
            var changePointDetectionTransformed = changePointDetectionModel.Transform(trainingDataView);

            var spikeDetectionPredictions = mlContext.Data.CreateEnumerable<OutputData>(spikeDetectionTransformed, reuseRowObject: false).ToArray();
            var changePointDetectionPredictions = mlContext.Data.CreateEnumerable<OutputData>(changePointDetectionTransformed, reuseRowObject: false).ToArray();

            var anomalyDetectionResults = new List<AnomalyDetectionResult>();
            
            for (var i = 0; i < trainingList.Count; i++)
            {
                anomalyDetectionResults.Add(new AnomalyDetectionResult
                {
                    TimeSeriesValue = trainingList[i].TimeSeriesValue,
                    Actual = trainingList[i].Actual,
                    SpikeAlert = spikeDetectionPredictions[i].Prediction[0],
                    SpikePrediction = spikeDetectionPredictions[i].Prediction[1],
                    SpikePValue = spikeDetectionPredictions[i].Prediction[2],
                    ChangePointAlert = changePointDetectionPredictions[i].Prediction[0],
                    ChangePointPrediction = changePointDetectionPredictions[i].Prediction[1],
                    ChangePointPValue = changePointDetectionPredictions[i].Prediction[2],
                    ChangePointMartingaleValue = changePointDetectionPredictions[i].Prediction[3]
                });
            }

            return new MachineLearningResult
            {
                AnomalyDetectionResults = anomalyDetectionResults
            };
        }

        private class InputData
        {
            public string TimeSeriesValue;

            public float Actual;
        }

        private class OutputData
        {
            [VectorType(3)] 
            public double[] Prediction { get; set; }
        }
    }
}
