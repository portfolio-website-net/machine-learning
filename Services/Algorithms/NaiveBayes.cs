﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MachineLearning.Services.Interfaces;
using MachineLearning.Services.Objects;
using MachineLearning.Services.Types;
using MachineLearning.Services.Helpers;
using Accord.MachineLearning.Rules;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.MachineLearning.DecisionTrees.Rules;
using Accord.MachineLearning.Bayes;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Filters;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math;
using Microsoft.ML;
using Microsoft.ML.AutoML;
using Microsoft.ML.Data;
using System.Data;

namespace MachineLearning.Services.Algorithms
{
    public class NaiveBayes : IAlgorithm
    {        
        private readonly MachineLearningServiceParameters _machineLearningServiceParameters;

        public NaiveBayes(MachineLearningServiceParameters machineLearningServiceParameters)
        {
            _machineLearningServiceParameters = machineLearningServiceParameters;
        }

        public MachineLearningResult GetResult()
        {
            var trainingDataset = ParseInput.Parse(_machineLearningServiceParameters.TrainingInput);  
            var valueInputDataset = ParseInput.Parse(_machineLearningServiceParameters.ValueInput);

            var columnNames = trainingDataset.Take(1).First().Select(x => x.ToString()).ToArray();
            var data = trainingDataset.Skip(1).ToArray();
            
            // Create a new codification codebook to
            // convert strings into discrete symbols
            Codification codebook = new Codification(columnNames, data);

            // Extract input and output pairs to train
            int[][] symbols = codebook.Transform(data);
            int[][] inputs = symbols.Get(null, 0, -1); // Gets all rows, from 0 to the last (but not the last)
            int[] outputs = symbols.GetColumn(-1);     // Gets only the last column

            // Create a new Naive Bayes learning
            var learner = new NaiveBayesLearning();

            Accord.MachineLearning.Bayes.NaiveBayes naiveBayes = learner.Learn(inputs, outputs);

            double[] probabilities = new double[0];
            string result = null;

            if (valueInputDataset.Any())
            {
                int[] instance = codebook.Transform(valueInputDataset.First().Select(x => x.ToString()).ToArray());
                int c = naiveBayes.Decide(instance);
                result = codebook.Revert(columnNames.Last(), c);
                probabilities = naiveBayes.Probabilities(instance);
            }

            return new MachineLearningResult
            {
                NaiveBayesResult = new NaiveBayesResult
                {
                    Probabilities = probabilities,
                    Result = result
                }
            };
        }        
    }
}
