﻿using System;
using System.IO;
using System.Reflection;

namespace MachineLearning.Services.Helpers
{
    public class PathHelper
    {
        public static string GetAbsolutePath(string relativePath)
        {
            var basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fullPath = Path.Combine(basePath, relativePath);
            if (!Directory.Exists(Path.GetDirectoryName(fullPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            }

            return fullPath;
        }
    }
}
