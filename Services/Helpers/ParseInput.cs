﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;

namespace MachineLearning.Services.Helpers
{
    public class ParseInput
    {        
        public static string[][] Parse(string input, char[] fieldSeparators = null)
        {
            if (fieldSeparators == null)
            {
                fieldSeparators = new char[] { ',', '\t'};
            }

            var result = new List<string[]>();
            if (!string.IsNullOrEmpty(input))
            {
                foreach (var line in input.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(line))
                    {
                        var items = line.Split(fieldSeparators).Select(x => x.Trim()).ToArray();
                        result.Add(items);
                    }
                }
            }

            return result.ToArray();
        }

        public static DataTable ConvertToDataTable(string[] columnNames, string[][] inputData)
        {            
            var dataTable = new DataTable();

            var columnNumber = 0;
            foreach (var columnName in columnNames)
            {               
                var columnValue = inputData.Skip(1).ToArray().First()[columnNumber];
                var isDouble = double.TryParse(columnValue, out var doubleValue);
                dataTable.Columns.Add(columnName, isDouble ? typeof(double) : typeof(string));
                columnNumber++;
            }

            foreach (var record in inputData.Skip(1).ToArray())
            {
                var row = dataTable.NewRow();
                int i = 0;
                foreach (var columnName in columnNames)
                {
                    var columnValue = record[i];
                    if (columnValue == "null")
                    {
                        columnValue = null;
                    }

                    row[columnName] = double.TryParse(columnValue, out double doubleValue) ? doubleValue.ToString() : columnValue;
                    i++;                    
                }                

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
    }
}
