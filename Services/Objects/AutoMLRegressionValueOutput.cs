﻿namespace MachineLearning.Services.Objects
{
    public class AutoMLRegressionValueOutput
    {
        public float Amount { get; set; }
    }
}
