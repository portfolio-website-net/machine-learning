﻿using System.Collections.Generic;

namespace MachineLearning.Services.Objects
{
    public class AutoMLMulticlassClassificationValueOutput
    {
        public List<MulticlassClassificationScore> MulticlassClassificationScores { get; set; }
    }
}
