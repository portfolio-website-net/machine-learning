﻿namespace MachineLearning.Services.Objects
{
    public class RegressionResult
    {
        public double LinearSlope { get; set; }

        public double LinearIntercept { get; set; }

        public string PolynomialExpression { get; set; }

        public double[] PolynomialWeights { get; set; }

        public double PolynomialIntercept { get; set; }

        public double PolynomialMeanSquaredError { get; set; }
    }
}
