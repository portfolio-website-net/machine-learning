﻿using System.Collections.Generic;

namespace MachineLearning.Services.Objects
{
    public class C45Result
    {
        public double Error { get; set; }

        public List<string> Rules { get; set; }
    }
}
