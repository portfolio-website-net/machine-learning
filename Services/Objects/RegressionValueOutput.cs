﻿namespace MachineLearning.Services.Objects
{
    public class RegressionValueOutput
    {
        public double LinearResult { get; set; }

        public double PolynomialResult { get; set; }
    }
}
