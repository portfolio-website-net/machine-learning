﻿using System.Collections.Generic;

namespace MachineLearning.Services.Objects
{
    public class NaiveBayesResult
    {
        public double[] Probabilities { get; set; }

        public string Result { get; set; }
    }
}
