﻿namespace MachineLearning.Services.Objects
{
    public class AprioriResult
    {
        public string Value1 { get; set; }

        public string Value2 { get; set; }

        public double Support { get; set; }

        public double Confidence { get; set; }        
    }
}
