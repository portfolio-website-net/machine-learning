﻿namespace MachineLearning.Services.Objects
{
    public class ValidationMetric
    {
        public double Accuracy { get; set; }

        public double AreaUnderRocCurve { get; set; }

        public double AreaUnderPrecisionRecallCurve { get; set; }

        public double F1Score { get; set; }

        public double PositivePrecision { get; set; }

        public double PositiveRecall { get; set; }

        public double NegativePrecision { get; set; }

        public double NegativeRecall { get; set; }

        public double MicroAccuracy { get; set; }

        public double MacroAccuracy { get; set; }

        public double LogLoss { get; set; }

        public double PerClassLogLoss1 { get; set; }

        public double PerClassLogLoss2 { get; set; }

        public double PerClassLogLoss3 { get; set; }

        public double LossFunction { get; set; }

        public double RSquared { get; set; }

        public double MeanAbsoluteError { get; set; }

        public double MeanSquaredError { get; set; }

        public double RootMeanSquaredError { get; set; }
    }
}
