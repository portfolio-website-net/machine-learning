﻿namespace MachineLearning.Services.Objects
{
    public class AnomalyDetectionResult
    {
        public string TimeSeriesValue { get; set; }

        public float Actual { get; set; }

        public double SpikeAlert { get; set; }

        public double SpikePrediction { get; set; }

        public double SpikePValue { get; set; }

        public double ChangePointAlert { get; set; }

        public double ChangePointPrediction { get; set; }

        public double ChangePointPValue { get; set; }

        public double ChangePointMartingaleValue { get; set; }
    }
}
