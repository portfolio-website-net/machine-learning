﻿using System.Collections.Generic;

namespace MachineLearning.Services.Objects
{
    public class MulticlassClassificationScore
    {
        public string Value { get; set; }

        public float Score { get; set; }
    }
}
