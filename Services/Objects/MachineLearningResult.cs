﻿using System;
using System.Collections.Generic;
using Accord.MachineLearning.Rules;

namespace MachineLearning.Services.Objects
{
    public class MachineLearningResult
    {
        public List<AprioriResult> AprioriResults { get; set; }

        public C45Result C45Result { get; set; }

        public NaiveBayesResult NaiveBayesResult { get; set; }

        public RegressionResult RegressionResult { get; set; }

         public RegressionValueOutput RegressionValueOutput { get; set; }

        public List<AnomalyDetectionResult> AnomalyDetectionResults { get; set; }

        public List<AutoMLRun> AutoMLRuns { get; set; }

        public AutoMLRun BestAutoMLRun { get; set; }

        public AutoMLBinaryValueOutput AutoMLBinaryValueOutput { get; set; }       

        public AutoMLMulticlassClassificationValueOutput AutoMLMulticlassClassificationValueOutput { get; set; }

        public AutoMLRegressionValueOutput AutoMLRegressionValueOutput { get; set; }
    }
}
