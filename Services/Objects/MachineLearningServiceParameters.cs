﻿using System.Collections.Generic;

namespace MachineLearning.Services.Objects
{
    public class MachineLearningServiceParameters
    {
        public string MachineLearningType { get; set; }

        public string TrainingInput { get; set; }

        public string TestingInput { get; set; }

        public string ValueInput { get; set; }

        public bool UseExistingModel { get; set; }

        public uint ExperimentTime { get; set; }

        public int HistoryLength { get; set; }
    }
}
