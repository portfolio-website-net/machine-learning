﻿namespace MachineLearning.Services.Objects
{
    public class AutoMLRun
    {
        public int RunNumber { get; set; }

        public string TrainerName { get; set; }

        public ValidationMetric ValidationMetrics { get; set; }

        public double? RuntimeInSeconds { get; set; }
    }
}
