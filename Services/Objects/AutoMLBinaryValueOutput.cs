﻿namespace MachineLearning.Services.Objects
{
    public class AutoMLBinaryValueOutput
    {
        public bool Result { get; set; }

        public float Score { get; set; }
    }
}
