﻿using MachineLearning.Services.Objects;

namespace MachineLearning.Services.Interfaces
{
    public interface IAlgorithm
    {
        MachineLearningResult GetResult();
    }
}
